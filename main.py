import os
import openpyxl
import serial
from datetime import datetime

directory = os.path.expanduser("~") + "\\Documents\\Code\\Git\\Decodeur\\data\\"
workbook = openpyxl.Workbook()
sheet = workbook.active

baudrate = 9600
row = 1
value = 0
temps = ""
hum = ""

if type == "a" or type == "A":

    port = input("Please enter your the number of your serial port(COMxx): ")
    ser = serial.Serial(
        port=port,
        baudrate=baudrate,
        timeout=30,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        xonxoff=True)

    try:
        while True:
            trame = ser.readline()
            messages = ""
            temps = (int(trame[4:8], 16) / 10)
            hum = (int(trame[12:14], 16) / 2)
            for i in range(1, 3) if row == 1 else []:
                cellule = sheet.cell(row=1, column=i + 1)
                if i == 1:
                    cellule.value = "Temp."
                elif i == 2:
                    cellule.value = "Hum."

            row += 1
            cellule = sheet.cell(row=row, column=2)
            cellule.value = temps
            cellule = sheet.cell(row=row, column=3)
            cellule.value = hum

            cellule = sheet.cell(row=row, column=1)
            duration = datetime.now().time()
            cellule.value = duration

            for i in range(0, 15):
                lenght = (14 + (i * 10))
                packets = trame[lenght:(lenght + 10)]

                if packets[2:4] == b'FA':
                    value = int(packets[4:], 16)
                    if row == 2:
                        cellule = sheet.cell(row=1, column=i + 4)
                        cellule.value = "Ch." + str(int(packets[0:2], 16)) + "(kΩ) "
                elif packets[2:4] == b'FE':
                    value = int(packets[4:], 16)
                    if row == 2:
                        cellule = sheet.cell(row=1, column=i + 4)
                        cellule.value = "Ch." + str(int(packets[0:2], 16)) + "(Ω) "
                else:
                    value = ""
                cellule = sheet.cell(row=row, column=i + 4)
                cellule.value = value
                messages = messages + str(value) + " "
            print(str(duration) + "   |   " + str(trame) + "             " + messages)

    except KeyboardInterrupt:
        name = input("Name the excel file you want to create or press \"n\":")
        location = directory + name + ".xlsx"
        if name != "n" or name != "":
            try:
                workbook.save(location)
            except PermissionError:
                print("File unavailable (probably already open)")
        pass
    ser.close()
